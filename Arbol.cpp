#include <iostream>
#include <unistd.h>
#include <fstream>
#include "Arbol.h"

using namespace std;

/*COnstructor*/
Arbol::Arbol(){}

/*Metodos*/
/*Metodo para crear nodo*/
Nodo* Arbol::crearNodo(double x){
  Nodo *temp;
  /*Se crea el nodo*/
  temp = new Nodo;
  /*Se le asigna un valor al nodo*/
  temp->numero = x;

  temp->izq = NULL;
  temp->der = NULL;
  temp->FE = 0;

  return temp;
}

/*Metodo para insertar un Nodo*/
void* Arbol::insertar(Nodo *&r, int dato, bool &crecido){
  /*Si el nodo r esta vacio, se inserta el nodo en esa posicion*/
  if(r != NULL){
    /*Agregar por izq*/
    if(dato < r->numero){
      insertar(r->izq, dato, crecido);
      if(crecido==true){
        switch(r->FE){
          case 1:
            r->FE = 0;
            crecido = false;
            break;

          case 0:
          r->FE = -1;
          break;

          case -1:
            nodo1 = r->izq;
            /*Rotacion Izq-Izq*/
            if(nodo1->FE <= 0){
              r->izq = nodo1->der;
              nodo1->der = r;
              r->FE = 0;
              r = nodo1;
            }
            /*Rotacion izq-der*/
            else{
              nodo2 = nodo1->der;
              r->izq = nodo2->der;
              nodo2->der = r;
              nodo1->der = nodo2->izq;
              nodo2->izq = nodo1;

              if(nodo2->FE == -1){
                r->FE = 1;
              }
              else{
                r->FE = 0;
              }

              if(nodo2->FE == 1){
                nodo1->FE = -1;
              }
              else{
                nodo1->FE = 0;
              }
              r = nodo2;
            }
            r->FE = 0;
            crecido = false;
            break;
        }
      }
    }
    /*Agregar por dere*/
    else{
      if(dato > r->numero){
        insertar(r->der, dato, crecido);
        if(crecido == true){
          switch(r->FE){
            case 1:
              nodo1 = r->der;
              /*Rotacion der-der*/
              if(nodo1->FE >= 0){
                r->der = nodo1->izq;
                nodo1->izq = r;
                r->FE = 0;
                r = nodo1;
              }
              /*Rotacion der-izq*/
              else{
                nodo2 = nodo1->izq;
                r->der = nodo2->izq;
                nodo2->izq = r;
                nodo1->izq = nodo2->der;
                nodo2->der = nodo1;

                if(nodo2->FE == 1){
                  r->FE = -1;
                }
                else{
                  r->FE = 0;
                }

                if(nodo2->FE == -1){
                  nodo1->FE = 1;
                }
                else{
                  nodo1->FE = 0;
                }
                r = nodo2;
              }
              r->FE = 0;
              crecido = false;
              break;
            case 0:
              r->FE = 1;
              break;
            case -1:
              r->FE = 0;
              crecido = false;
              break;
          }
        }
        /*NUmero repetido*/
        else{
          cout << "Numero repetido!!" << endl;
        }
      }
    }
  }
  else{
    r = crearNodo(dato);
    crecido = true;
    cout << "Nodo Agregado" << endl;
  }
}

/*Imprimir preorden*/
void Arbol::preorden(Nodo *p){
  if(p != NULL){
    cout << "[" << p->numero << "]" << " - ";
    /*Llamada recursiva a la funcion*/
    preorden(p->izq);
    preorden(p->der);
  }
}

/*Imprimir inorden*/
void Arbol::inorden(Nodo *p){
  if(p != NULL){
    /*Llamada recursiva a la funcion*/
    inorden(p->izq);
    cout << "[" << p->numero << "]" << " - ";
    /*Llamada recursiva a la funcion*/
    inorden(p->der);
  }
}

/*Imprimir postorden*/
void Arbol::postorden(Nodo *p){
  if(p != NULL){
    /*Llamada recursiva a la funcion*/
    postorden(p->izq);
    postorden(p->der);
    cout << "[" << p->numero << "]" << " - ";
  }
}

/*Eliminacion de un nodo*/
void Arbol::eliminar(Nodo *&r, int dato, bool &disminuyo){
  if(r!=NULL){
    /*buscar numero por izq*/
    if(dato < r->numero){
      eliminar(r->izq, dato, disminuyo);
      reestructuracionIzq(r, disminuyo);
    }
    else{
      /*buscar numero por derecha*/
      if(dato > r->numero){
        eliminar(r->der, dato, disminuyo);
        reestructuracionDer(r, disminuyo);
      }
      else{
        otro = r;
        disminuyo = true;
        if(otro->der == NULL){
          r = otro->izq;
        }
        else{
          if(otro->izq == NULL){
            r = otro->der;
          }
          else{
            aux1 = r->izq;
            disminuyo = false;
            while(aux1->der != NULL){
              aux2 =aux1;
              aux1 = aux1->der;
              disminuyo = true;
            }
            r->numero = aux1->numero;
            otro = aux1;
            if(disminuyo == true){
              aux2->der = aux1->izq;
            }
            else{
              r->izq = aux1->izq;
            }
            reestructuracionDer(r->izq, disminuyo);
          }
        }
        delete otro;
      }

    }
  }
  else{
    cout << "Numero inexistente" << endl;
  }

}

/*reestructuracion por Izq*/
void Arbol::reestructuracionIzq(Nodo *&r, bool &disminuyo){
  if(disminuyo == true){
	   switch(r->FE){
       case 1:
        nodo1 = r->der;
        /*rotacion izq-izqr*/
        if(nodo1->FE >= 0){
          r->der = nodo1->izq;
          nodo1->izq = r;

          if(nodo1->FE == 0){
            r->FE = 1;
            nodo1->FE = -1;
            disminuyo = false;
          }
          if(nodo1->FE == 1){
            r->FE = 0;
            nodo1->FE = 0;
          }
          r = nodo1;
        }
        /*rotacion izq-der*/
        else{
          nodo2 = nodo1->izq;
          r->der = nodo2->izq;
          nodo2->izq = r;
          nodo1->izq = nodo2->der;
          nodo2->der = nodo1;

          if(nodo2->FE == 1){
            r->FE = -1;
          }
          else{
            r->FE = 0;
          }
          if(nodo2->FE == -1){
            nodo1->FE = 1;
          }
          else{
            nodo1->FE = 0;
          }
          r = nodo2;
          nodo2->FE = 0;
        }
        r->FE = 0;
        disminuyo = false;
        break;
      case 0:
        r->FE = -1;
        disminuyo = false;
        break;
      case -1:
        r->FE = 0;
        break;
     }
   }
}

/*reestructuracion por Der*/
void Arbol::reestructuracionDer(Nodo *&r, bool &disminuyo){
  if(disminuyo == true){
	   switch(r->FE){
       case -1:
        nodo1 = r->izq;
        /*rotacion izq-izqr*/
        if(nodo1->FE >= 0){
          r->izq = nodo1->der;
          nodo1->der = r;

          if(nodo1->FE == 0){
            r->FE = -1;
            nodo1->FE = 1;
            disminuyo = false;
          }
          if(nodo1->FE == -1){
            r->FE = 0;
            nodo1->FE = 0;
          }
          r = nodo1;
        }
        /*rotacion izq-der*/
        else{
          nodo2 = nodo1->der;
          r->izq = nodo2->der;
          nodo2->der = r;
          nodo1->der = nodo2->izq;
          nodo2->izq = nodo1;

          if(nodo2->FE == -1){
            r->FE = 1;
          }
          else{
            r->FE = 0;
          }
          if(nodo2->FE == 1){
            nodo1->FE = -1;
          }
          else{
            nodo1->FE = 0;
          }
          r = nodo2;
          nodo2->FE = 0;
        }
        r->FE = 0;
        disminuyo = false;
        break;
      case 0:
        r->FE = 1;
        disminuyo = false;
        break;
      case 1:
        r->FE = 0;
        break;
     }
   }
}

/*Metodo para buscar un nodo*/
bool Arbol::busqueda(Nodo *r, double numero){
  /*Si el numero es menor, se busca por la izq*/
  if(numero < r->numero){
    /*Si el nodo por la izq apunta a nulo*/
    if(r->izq == NULL){
      cout << "No existe el numero" << endl;
      return false;
    }
    else{
      /*LLamada recursiva con el valor apuntado por izq*/
      busqueda(r->izq, numero);
    }
  }

  /*Si es mauor, se busca por der*/
  else if(numero > r->numero){
    /*Si apunta a nulo por derecha*/
    if(r->der == NULL){
      cout << "No existe el numero" << endl;
      return false;
    }
    else{
      /*LLamada recursiva con el valor apuntando por der*/
      busqueda(r->der, numero);
    }
  }
  else{
    cout << "Numero encontrado" << endl;
    return true;
  }
}


/*Metodo para crear el Archvivo .txt*/
void Arbol::crearGrafo(Nodo *r){
	/*Creacion del archivo*/
	ofstream archivo("grafo.txt", ios::out);
	if (archivo.fail()){
		cout << "No se pudo abrir archivo!" << endl;
	}
	else{
		/*Escritura en el archivo .txt*/
		archivo << "digraph G {\n";
		archivo << "node [style=filled fillcolor=red] ;\n" ;
		/*LLamada al metodo para ingresar la info del arbol*/
		escribirtxt(r, archivo);
		archivo << "}";
		archivo.close();
		/*Generacion de la imagen a traves del archivo .txt*/
		system("dot -Tpng -ografo.png grafo.txt &");
		/*Apertura de la imagen*/
		system("eog grafo.png &");
		}
}

/*Metodo para escribir la info del arbol en el archivo .txt*/
void Arbol::escribirtxt(Nodo *r, ofstream &archivo){
	/*Recorrido del arbol en preorden y se agregan datos al archiv .txt*/
	if( r != NULL){
		if (r->izq != NULL) {
			archivo << r->numero << "->" << r->izq->numero << ";\n";
		}
	  else {
			archivo << '"' << r->numero << "i" << '"' << " [shape=point];\n";
			archivo << r->numero << "->" << '"' << r->numero << "i" << '"' << ";\n" ;
		}
		if (r->der != NULL) {
			archivo << r->numero << "->" << r->der->numero << ";\n";
		}
		else {
			archivo << '"' << r->numero << "d" << '"' << " [shape=point];\n";
			archivo << r->numero << "->" << '"' << r->numero << "d" << '"' <<";\n";
  	}
    escribirtxt(r->der, archivo);
		escribirtxt(r->izq, archivo);
	}
}
