#include <iostream>
using namespace std;

#ifndef ARBOL_H
#define ARBOL_H

typedef struct Nodo{
  double numero=0;
  int FE;
  struct Nodo *izq;
  struct Nodo *der;
} Nodo;

class Arbol{
  private:
    Nodo *nodo1 = new Nodo;
    Nodo *nodo2 = new Nodo;

    /*NOdos auxiliares para la funcion eliminar*/
    Nodo *otro = NULL;
    Nodo *aux1 = NULL;
    Nodo *aux2 = NULL;
  public:
    /*Constructor*/
    Arbol();
    /*Metodos*/
    Nodo *crearNodo(double x);
    void *insertar(Nodo *&r, int dato, bool &crecido);
    void preorden(Nodo *p);
    void inorden(Nodo *p);
    void postorden(Nodo *p);
    void eliminar(Nodo *&r, int dato, bool &disminuyo);
    bool busqueda(Nodo *r, double numero);
    void reestructuracionIzq(Nodo *&r, bool &disminuyo);
    void reestructuracionDer(Nodo *&r, bool &disminuyo);
    void crearGrafo(Nodo *r);
    void escribirtxt(Nodo *r, ofstream &archivo);


};
#endif
